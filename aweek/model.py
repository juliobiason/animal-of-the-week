#!/usr/bin/python
# -*- encoding: utf-8 -*-

from mongoengine import Document
from mongoengine import DateTimeField
from mongoengine import StringField
from mongoengine import ListField
from mongoengine import ReferenceField


class Animal(Document):
    genus = StringField()
    species = StringField()
    date = DateTimeField()


class User(Document):
    login = StringField(primary_key=True)
    password = StringField()
    fullname = StringField()
    joined = DateTimeField()
    future_options = ListField(ReferenceField(Animal))


class Picks(Document):
    date = DateTimeField()
    who = ReferenceField(User)
    animal = ReferenceField(Animal)
