#1/usr/bin/python
# -*- encoding: utf-8 -*-

from flask import Blueprint
from flask import render_template
from flask.views import MethodView

from ..model import Picks
from ..model import User

import logging
import datetime

main = Blueprint('main',
                 __name__,
                 template_folder='../templates')

_log = logging.getLogger('aweek.main')


class ListView(MethodView):
    def get(self):
        # previous selections
        today = datetime.datetime.utcnow()
        last_4_weeks = today - datetime.timedelta(days=7 * 4)

        query = {'date': {'$gte': last_4_weeks}}

        objs = Picks.objects(query).order_by('+date')
        _log.debug('{} entries'.format(objs.count()))

        # people who will pick it next
        future = []
        all_users = User.objects.all().order_by('+joined')

        last_user = None
        if objs.count() > 0:
            gather = False
        else:
            gather = True   # no picks in the db, so we get everyone

        for user in all_users:
            _log.debug('Gathering? {}'.format('YES' if gather else 'NO'))
            if gather:
                future.append(user)
            elif user.pk == last_user:
                gather = True

            if len(future) >= 4:
                break

        if len(future) < 4:
            needed = 4 - len(future)
            future.extend(all_users[:needed])

        _log.debug(future)

        return render_template('index.html', previous=objs, next=future)


main.add_url_rule('/', view_func=ListView.as_view('list'))
