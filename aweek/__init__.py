from flask import Flask
from flask.ext.mongoengine import MongoEngine

import logging

_log = logging.getLogger('aweek')

app = Flask(__name__)
app.config["MONGODB_SETTINGS"] = {'DB': "aweek"}
app.config["SECRET_KEY"] = "KeepThisS3cr3t"
db = MongoEngine(app)


def register_blueprints(app):
    from main.views import main
    # from provider.views import provider

    # app.register_blueprint(geoconfiguration, url_prefix='/geoconfiguration')
    app.register_blueprint(main)


register_blueprints(app)
_log.debug('Blueprints registered')

if __name__ == '__main__':
    app.run()
