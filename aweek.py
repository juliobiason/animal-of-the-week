#!/usr/bin/python
# -*- encoding: utf-8 -*-

import os
import sys
import logging


sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from flask.ext.script import Manager, Server
from aweek import app

manager = Manager(app)

# Turn on debugger by default and reloader
manager.add_command("runserver", Server(
    use_debugger=True,
    use_reloader=True,
    host='0.0.0.0')
)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    manager.run()
